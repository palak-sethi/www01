import React from "react"
import { graphql } from "gatsby"
import { Container } from "react-bootstrap"

import Layout from "../components/layout"
import SEO from "../components/seo"
import PostsList from "../components/postlist"
import Slider from "../components/slider"

const CategoryTemplate = ({ location, pageContext, data }) => {
  const { category } = pageContext
  return (
    <Layout location={location} title={`Kategória "${category}"`}>
      <SEO title={`Kategória "${category}"`} />

      <Slider
        title={`Kategória "${category}"`}
        subtitle="Minden cikk az adott kategóriában"
      />

      <Container fluid>
        <PostsList postEdges={data.allMarkdownRemark.edges} />
      </Container>
    </Layout>
  )
}

export const pageQuery = graphql`
  query CategoryPage($category: String) {
    allMarkdownRemark(
      limit: 1000
      filter: { fields: { category: { eq: $category } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slug
            category
          }
          excerpt
          timeToRead
          frontmatter {
            title
            description
            date
          }
        }
      }
    }
  }
`

export default CategoryTemplate
