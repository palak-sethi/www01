import React from "react"
import "../scss/bootstrap-custom.scss"
import "../lib/animate-3.7.2.css"

import Header from "./header"
import Footer from "./footer"
import LinkBand from "./linkband"

const Layout = ({ showEu2020, children }) => {
  return (
    <>
      <Header showEu2020={showEu2020} />

      <main>{children}</main>

      <LinkBand />

      <Footer />
    </>
  )
}

export default Layout
