import React from "react";
import Helmet from "react-helmet";
import { graphql, useStaticQuery } from "gatsby";
import PropTypes from "prop-types";

function SEO({ description, lang, meta, title }) {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
            siteUrl
            image
            social {
              twitter
            }
          }
        }
      }
    `,
  )

  const metaDescription = description || site.siteMetadata.description

  return (
    <Helmet
      htmlAttributes={{
        lang,
      }}
      title={title}
      titleTemplate={`%s | ${site.siteMetadata.title}`}
      meta={[
        {
          name: "charSet",
          content: "utf-8",
        },
        {
          name: "keywords",
          content: "segítőkutya, terápiáskutya, rohamjelzőkutya, mozgássérültet, segítő, kutya, AURA, Alapítvány",
        },
        {
          name: `description`,
          content: metaDescription,
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:description`,
          content: metaDescription,
        },
        {
          property: `og:type`,
          content: `website`,
        },
        {
          property: `og:url`,
          content: `${site.siteMetadata.siteUrl}`,
        },
        {
          property: `og:image`,
          content: `${site.siteMetadata.siteUrl}${site.siteMetadata.image}`,
        },
        {
          name: `twitter:card`,
          content: `summary`,
        },
        {
          name: `twitter:creator`,
          content: site.siteMetadata.author,
        },
        {
          name: `twitter:title`,
          content: title,
        },
        {
          name: `twitter:description`,
          content: metaDescription,
        },
        {
          name: `twitter:site`,
          content: `@${site.siteMetadata.social.twitter}`,
        },
        {
          name: `twitter:image`,
          content: `${site.siteMetadata.siteUrl}${site.siteMetadata.image}`,
        },
      ].concat(meta)}
    >
      <script type="application/ld+json">
        {`
        {
          "@context" : "https://schema.org",
          "@graph" : [
            {
              "@type" : "Organization",
              "@id" : "http://segitokutya.hu/#organization",
              "url" : "${site.siteMetadata.siteUrl}",
              "name" : "${site.siteMetadata.title}",
              "sameAs" : [
                "http://segitokutya.hu",
                "https://www.facebook.com/AURA-Seg%C3%ADt%C5%91-Kutya-Alap%C3%ADtv%C3%A1ny-ad%C3%B3sz%C3%A1m-18995736-1-09-164164377025485",
                "https://www.instagram.com/aurasegitokutya/",
                "https://www.youtube.com/user/aurasegitokutya"
              ],
              "logo" : {
                "@type" : "ImageObject",
                "@id" : "http://segitokutya.hu/#logo",
                "inLanguage" : "hu",
                "url" : "${site.siteMetadata.siteUrl}${site.siteMetadata.image}",
                "width" : 714,
                "height" : 714,
                "caption" : "${site.siteMetadata.title}"
              },
              "image" : {
                "@id" : "http://segitokutya.hu/#logo"
              }
            },
            {
              "@type" : "WebSite",
              "@id" : "http://segitokutya.hu/#website",
              "url" : "${site.siteMetadata.siteUrl}",
              "name" : "${site.siteMetadata.title}",
              "description" : "${metaDescription}",
              "inLanguage" : "hu",
              "publisher" : {
                "@id" : "http://segitokutya.hu/#organization"
              }
            }
          ]
        }
        `}
      </script>
      <link rel="preconnect dns-prefetch" href="//www.google.com"/>
      <link rel="preconnect dns-prefetch" href="//fonts.googleapis.com"/>
      <link rel="preconnect dns-prefetch" href="//cse.google.com"/>
      <link rel="canonical" href="https://www.segítőkutya.hu/"/>
      <script async src="https://cse.google.com/cse.js?cx=015378035370168471795:t5mnnuhuylt"/>
    </Helmet>
  )
}

SEO.defaultProps = {
  lang: `hu`,
  meta: [],
  description: ``,
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}

export default SEO
