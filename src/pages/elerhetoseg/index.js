import React from "react"

import { graphql } from "gatsby"
import { FaEnvelope, FaMapMarker, FaPhone } from "react-icons/fa"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import Slider from "../../components/slider"
import Container from "react-bootstrap/Container"
import Form from "react-bootstrap/Form"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Button from "react-bootstrap/Button"

const IndexPage = ({ data }) => {
  const contact = data.allContactYaml.edges[0].node.contact

  return (
    <Layout>
      <SEO title="Elérhetőségek" />

      <Slider title="Elérhetőségek" subtitle="Lépjen velünk kapcsolatba!" />

      <Container style={{ paddingBottom: "2em" }}>
        <h4>Írjon nekünk!</h4>
        <Form
          method="post"
          action="https://getform.io/f/bf89946a-672a-4205-bda9-00f5fdd65c1c"
        >
          <Row style={{ paddingBottom: "1em" }}>
            <Col>
              <Form.Control placeholder="Név" name="name" />
            </Col>
            <Col>
              <Form.Control placeholder="Email" name="email" />
            </Col>
          </Row>
          <Row style={{ paddingBottom: "1em" }}>
            <Col>
              <Form.Control placeholder="Tárgy" name="subject" />
            </Col>
          </Row>
          <Row style={{ paddingBottom: "1em" }}>
            <Col>
              <Form.Control as="textarea" placeholder="Üzenet" name="message" />
              <Form.Text className="text-muted">
                Neveket és email címeket sosem adunk ki harmadik félnek.
              </Form.Text>
            </Col>
          </Row>
          <Row style={{ paddingBottom: "1em" }}>
            <Col>
              <Button variant="primary" type="submit">
                Üzenet elküldése
              </Button>
            </Col>
          </Row>
        </Form>
      </Container>

      <Container>
        <Row>
          <Col>
            <h4>Címünk</h4>
            <p style={{ lineHeight: "2.5em" }}>
              <FaMapMarker size="1.5em" /> {contact.address} <br />
              <FaEnvelope size="1.5em" /> <a target="_blank" rel="noreferrer" href={"mailto:" + contact.email}>{contact.email}</a> <br />
              <FaPhone size="1.5em" /> {contact.phone} ({contact.phonePerson})
            </p>
          </Col>
          <Col>
            <h4>Térkép</h4>
            <iframe
              title={contact.address}
              src={contact.mapSrc}
              width="555"
              height="300"
              frameBorder="0"
              style={{ border: 0 }}
              allowFullScreen=""
            ></iframe>
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query {
    allContactYaml {
      edges {
        node {
          contact {
            address
            email
            phone
            phonePerson
            mapSrc
          }
        }
      }
    }
  }
`
