import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import { Container } from "react-bootstrap"

import Layout from "../../components/layout"
import SEO from "../../components/seo"
import PostsList from "../../components/postlist"
import Slider from "../../components/slider"

const CategoryTemplate = ({ location }) => {
  const data = useStaticQuery(graphql`
    query {
      q: allMarkdownRemark(
        limit: 1000
        filter: { fields: { category: { eq: "gyik" } } }
        sort: { fields: [frontmatter___title], order: ASC }
      ) {
        totalCount
        edges {
          node {
            fields {
              slug
              category
            }
            excerpt
            timeToRead
            frontmatter {
              title
              description
              date
            }
          }
        }
      }
    }
  `)

  return (
    <Layout location={location} title={`GYIK`}>
      <SEO title={`GYIK`} />

      <Slider title="GYIK" subtitle="Gyakran ismételt kérdések" />

      <Container fluid>
        <PostsList postEdges={data.q.edges} />
      </Container>
    </Layout>
  )
}

export default CategoryTemplate
