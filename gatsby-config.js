module.exports = {
  siteMetadata: {
    title: "AURA Segítő Kutya Alapítvány",
    description:
      "Az AURA Alapítvány kitűzött célja, hogy a kutyás terápia módszerét és a segítő kutyák alkalmazásának előnyeit széles körben megismertesse és elterjessze",
    author: "AURA",
    siteUrl: "https://www.xn--segtkutya-i5a51i.hu",
    image: "/aura-logo.png",
    social: {
      twitter: "AuraSegitokutya",
      facebook:
        "AURA-Segítő-Kutya-Alapítvány-adószám-18995736-1-09-164164377025485",
      instagram: "aurasegitokutya",
      youtube: "aUClGTFeO7F-lAkJoPABwcm5Q",
    },
  },
  plugins: [
    {
      resolve: "gatsby-plugin-react-helmet",
    },
    {
      resolve: "gatsby-plugin-sitemap",
    },
    {
      resolve: "gatsby-plugin-sass",
    },
    {
      resolve: "gatsby-plugin-robots-txt",
    },
    {
      resolve: "gatsby-plugin-react-svg",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "src/images",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "posts",
        path: "content/post",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "yaml",
        path: "content/yaml",
      },
    },
    {
      resolve: `gatsby-plugin-typescript`,
      options: {
        isTSX: true,
        // jsxPragma: `jsx`,
        allExtensions: true,
      },
    },
    {
      resolve: "gatsby-transformer-yaml",
    },
    {
      resolve: "gatsby-transformer-sharp",
    },
    {
      resolve: "gatsby-plugin-sharp",
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-images",
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: "gatsby-remark-responsive-iframe",
            options: {
              wrapperStyle: "margin-bottom: 1.0725rem",
            },
          },
          "gatsby-remark-copy-linked-files",
          "gatsby-remark-smartypants",
        ],
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-167428519-1",
        head: false,
        anonymize: true,
        respectDNT: true,
        exclude: ["/preview/**", "/do-not-track/me/too/"],
        pageTransitionDelay: 0,
        // Enables Google Optimize using your container Id
        // optimizeId: "YOUR_GOOGLE_OPTIMIZE_TRACKING_ID",
        // Enables Google Optimize Experiment ID
        // experimentId: "YOUR_GOOGLE_EXPERIMENT_ID",
        // Set Variation ID. 0 for original 1,2,3....
        // variationId: "YOUR_GOOGLE_OPTIMIZE_VARIATION_ID",
        // Defers execution of google analytics script after page load
        defer: false,
        // Any additional optional fields
        // sampleRate: 5,
        // siteSpeedSampleRate: 10,
        // cookieDomain: "example.com",
      },
    },
    {
      resolve: "gatsby-plugin-google-fonts",
      options: {
        fonts: ["Roboto", "Open Sans:300,400,400i,700"],
        display: "swap",
      },
    },
    {
      resolve: "gatsby-plugin-categories",
      options: {
        templatePath: `${__dirname}/src/templates/category.js`,
      },
    },
    {
      resolve: "gatsby-plugin-tags",
      options: {
        templatePath: `${__dirname}/src/templates/tag.js`,
      },
    },
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        name: "AURA Segítő Kutya Alapítvány",
        short_name: "AURA",
        start_url: "/",
        background_color: "#ffffff",
        theme_color: "#4bb749",
        display: "AURA",
        icon: "src/images/aura-notext-big.png",
      },
    },
  ],
}
