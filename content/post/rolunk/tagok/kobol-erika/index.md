---
title: Köböl Erika
date: "2020-06-13"
description: "... Szeged első terápiás kutyája"
category: "Tagok"
tags:
  - tagok
---

![Erika 1](alba2.jpg)

Köböl Erika vagyok, gyógypedagógus, habilitációs kutya kiképző, felvezető. 
2009\. óta vagyok tagja az Aura baráti és szakmai közösségének. 

Két vizsgázott terápiás kutyám van. 
Mázli 12 éves elmúlt, ő volt Szeged első terápiás kutyája, ma már boldog 
nyugdíjas éveit tölti. Alba a testvére, 8 éves, aktívan dolgozó remek munkatárs.

Nekik köszönhetően a gyógypedagógia, pedagógia, szociális munka sok területére
eljuthattam, mint gyakorló gyógypedagógus és egyben felvezető.
Segítségükkel tudásomat átadhatom a SZTE JGYPK Gyógypedagógus-képző Intézetének
munkatársaként is.

![Erika 2](alba.jpg)
