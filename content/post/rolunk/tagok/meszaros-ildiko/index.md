---
title: Mészáros Ildikó
date: "2020-06-14"
description: "... Igyekszünk mosolyt csalni az emberek arcára"
category: "Tagok"
tags:
  - tagok
---

![Ildikó](meszarosildiko.jpg)

Mészáros Ildikó vagyok, gyógypedagógus. Hosszú évek óta dolgozom együtt az idős terápiás leonbergimmel, az óriási Trinityvel. Ő egy igazi született terápiás kutya, aki imádja az embereket, mindene a munkája. Számos helyen megfordultunk már az évek során, kezdve az idősek otthonától, a gyermekpszichiátrián át egészen jó néhány óvodáig. Igyekszünk mosolyt csalni az emberek arcára terápiás párosként, bárhol is vagyunk, legyen az egy egyetemen, vagy akár szimplán a buszon. Bármilyen helyszínen vagyunk is, igyekszem tudásommal, kreativitásommal kiegészíteni, segíteni Trinity nyugodt, mégis vidám személyiségének kibontakozását, fejlesztő munkáját.

![Ildikó](meszarosildiko2.jpg)



