---
title: Dr. Kulcsár Andrea
date: "2020-06-14"
description: " Dr. Kulcsár Andrea, gyermekgyógyász, gyermekneurológus és gyermek-rehabilitációs
szakorvos.."
category: "Tagok"
tags:
  - tagok
---

![Andrea](kulcsarandrea4.jpg)

Dr. Kulcsár Andrea vagyok, gyermekgyógyász, gyermekneurológus és gyermek-rehabilitációs
szakorvos. Debrecenben, a Kenézy Kórház Gyermekrehabilitációs részlegén dolgozom orvosként illetve magánrendelést is végzek. A kutyás terápiával a rehabilitációs pályafutásom elején ismerkedtem meg, amikor az intézetünkbe elkezdtek járni a terápiás kutyák. Mivel mindig nagyon szerettem ezeket az állatokat, csak sajnos sokáig nem volt lehetőségem arra hogy sajátot neveljek, a terápiás kutyákkal (Zerge, Saci, Luna és a többiek) való megismerkedésem megadta a „kegyelemdöfést” és így vállaltam az első kutyámat, Lizzyt aki már 10,5 éves, golden retriever hölgy. Kollégáim biztattak hogy próbáljuk meg kiképezni terápiás kutyának, és hát amikor a rendelésemen először bizonyított, és az addig ismeretlen diagnózisú kisgyerek elkezdett vele „beszélgetni” a maga sajátos nyelvén, és egyértelművé vált, hogy mi a probléma, akkor elhatároztam hogy belevágok. Nagyon sok segítséget kaptam a konduktor kolléganőmtől aki kutyakiképző és végül az Aura Alapítvány színeiben és támogatásával Lizzyvel levizsgáztunk. Ő főleg a rendelésemen segít a gyerekek vizsgálatában, illetve heti 2 órát tart a Gyermek Rehabilitációs Központban, most már „nyugdíjasként”.
Mivel nagyon közeli kapcsolatba kerültünk Kavics kutyával, és ő egy szintén remek partnerrel,
Bigyóval „frigyre lépett”, boldogságomra megszületett egy kiváló alom tagjaként Johnny kutyám,
akiért mélyen hálás vagyok Krajczár Gabinak, Kavics gazdájának valamint Nagy Lajosnak és nem utolsósorban az én legdrágább kutya barátomnak… Johnny 3 éves labrador, most képződik, és az aktivitásban lassan átveszi a stafétabotot Lizzytől.
Szorgalmasan dolgozunk, remélem sok gyerek és család örömére (és persze a Gyermekrehabilitáció megelégedésére)…

![Andrea](kulcsarandrea.jpg)


