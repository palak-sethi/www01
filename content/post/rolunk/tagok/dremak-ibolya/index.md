---
title: Dremák Ibolya
date: "2020-06-14"
description: "... kutyák szemével látni a világot"
category: "Tagok"
tags:
  - tagok
---

Dremák Ibolyának hívnak. 2018-ban szereztem képesítést mint Habilitációs kutya kiképző. Célom az volt, hogy megtanuljam a kutyák szemével látni a világot, ezáltal megteremtve magam és mások számára az esélyt, hogy javítsunk ember és kutya viszonyán, valamint könnyebbé tegyük az életet.

![Ibolya](dremakibolya.jpg)


