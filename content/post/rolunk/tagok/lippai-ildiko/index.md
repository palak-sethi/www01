---
title: Lippai Ildikó
date: "2020-06-14"
description: "... nem csak a társam, a tanítómesterem is"
category: "Tagok"
tags:
  - tagok
---

2 éve dolgozom Wendy-vel. Ő nem csak a társam, hanem a tanítómesterem is. Ő mutatta meg
nekem, milyen sok munka egy terápiás kutya kiképzése, milyen összeszedettnek kell lennie egy
terapeutának és mennyire megtérül a foglalkozásokba fektetett energia. Amikor láttam, hogy egy kutya gyógyítani képes, gyerekek nevetnek a közelünkben és a sötét világosra változik, tudtam, én ezt akarom csinálni. Hogy egy simogatásnak mekkora ereje van, számomra ez az igazi varázslat.


![Ildikó](lippaiildiko.jpg)


