---
title: Berta-Nagy Julianna
date: "2020-06-14"
description: "... Kópé barátja"
category: "Tagok"
tags:
  - tagok
---

![Julianna 1](bertanagyjulianna2.jpg)

Berta-Nagy Julianna magyartanár, gyógypedagógus, logopédus, habilitációs- és mozgássérült
kutyakiképző vagyok hivatalos minőségemben.
A gyerekek szerint Kópé barátja. Kópé jelenleg 11 éves angol cocker spániel, akinek mindent
jelent, ha gyerekek között lehet. 9 éve dolgozik aktívan, számtalan csoportban és táborban
segített és szerzett örömet fiataloknak és gyerekeknek egyaránt. Dolgoztunk és dolgozunk
tipikus fejlődésmenetű kisiskolásokkal, halmozottan sérült gyerekekkel és fiatal felnőttekkel
egyaránt, valamint autista és értelmileg akadályozott óvodásokkal. Kópé egy hatalmas
munkabírású, mindig vidám kutya. Egy igazi kópé – hisz a név kötelez.


![Julianna 2](bertanagyjulianna3.jpg)
