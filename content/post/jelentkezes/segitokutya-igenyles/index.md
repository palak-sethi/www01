---
title: "Hogyan kaphatok segítő kutyát?"
date: "2020-06-20"
description: "Igényléstől a segítőkutyáig"
category: "Jelentkezés"
tags:
  - segitokutya
  - jelentkezes
---

1\. Írjon nekünk egy e-mailt a [segitokutya.aura@gmail.com](mailto:segitokutya.aura@gmail.com) címre! 
    Az emailban pontosan írja le a következőket:

1. Kinek igényli a segítő kutyát?
2. Hol (melyik városban, országban) lakik?
3. Mi a betegsége, fogyatékossága, vagy problémája, nehézsége?
4. Miben kellene, hogy segítsen a kutya?
5. Mi az Ön neve, telefonszáma, elérhetősége, amin felvehetjük Önnel a kapcsolatot?
 
2\. Ha szükséges, jelentkezése után néhány nappal felhívjuk Önt és pontosítjuk, milyen típusú segítőkutyára van szüksége.

3\. Küldünk egy jelentkezési lapot, melyben néhány fontos kérdésre kell választ adnia. A jelentkezési visszatérte és 
    feldolgozása után:

4\. Személyes beszélgetésre hívjuk meg a segítő kutyát igénylőt és a vele egy háztartásban élő családtagokat, ahol beszélgetünk

1. életkörülményeiről (hol lakik, mivel foglalkozik, tart-e háziállatot)
2. életmódjáról (mi a hobbija, mi a napi rutin)
3. milyen nehézségekkel és előnyökkel jár egy segítő kutyával együtt élni
4. milyen költségeket kell fizetni
5. bemutatjuk, hogyan képezzük a kutyákat és a kutyát igénylőket (kb. 30 perc)
6. ki kell tölteni egy pszichológiai tesztet (kb. 15 perc)
7. beszélgetés egy pszichológussal (kb. 30 perc)

5\. Az alapítvány szakemberei kiértékelik az információkat és döntenek arról, tudnak-e Önnek segítő kutyát képezni.

6\. Szükség esetén az alapítvány szakembere meglátogatja Önt otthonában, hogy az életkörülményeiről még pontosabb képet kapjon. Így még inkább az Önnek legmegfelelőbb kutyát tudjuk kiválasztani és kiképezni.

7\. Szerződéskötés az alapítvány és Ön között segítőkutya kiképzésre vonatkozóan.

8\. Vagy a már kiképzés alatt álló kutyák közül választunk Önnek kutyát,
vagy új kölyköt keresünk. A kölyköket 3 naposan és 6 hetesen teszteljük, hogy az Ön igényeinek leginkább megfelelő kutyát választhassuk ki.

9\. A kiválasztott kölyök az alapítvány egyik segítőkutya kiképzőjéhez kerül. A kiképző a nap 24 órájában az Ön kutyájával él együtt. Így egy jól szocializált és az Ön igényeihez szabott kiképzést kap a kutyája.

10\. Ön és családtagjai egyeztetett terv szerint látogathatják a kutyát a kezdetektől. Előre meghatározott program szerint Ön megtanulja a látogatások során, hogyan tudja irányítani a kutyát. (Hogyan motiválja, gátolja a kutyát, milyen vezényszavakat, jeleket használjon, milyen módon kommunikáljon a kutyával.)
A kiképzés végső szakaszában a szimulációs házunkban begyakorolhatja az otthonában gyakran előforduló élethelyzeteket. (pl. kutya táplálása, itatása, főzés-étkezés, öltözködés, tanulás, játék a gyerekekkel, mosás, takarítás, + speciális feladatok: ajtó nyitás-zárás, villanykapcsolás, telefon vagy TV kapcsoló gazdihoz vitele, + közlekedés városban, bevásárlóközpontban, buszon, villamoson, autóval, stb.)

11\. Segítőkutya vizsga (lásd [27/2009. SZMM. Rendelet melléklete](/tudastar/szmm-27-2009/) és a [MATESZE segítőkutya vizsgaszabályzatok](http://www.matesze.hu/letoltheto.html)) a tanúsítvány kiállításának feltétele.

12\. Sikeres vizsga esetén megkapja a Tanúsítványt és kutyája végleg Önhöz költözik.

13\. Utógondozás: A kutya élete végéig nyomon követjük a gazda-kutya életét. Szükség szerint segítünk az új vagy problémás helyzetek megoldásában, illetve a kutya nem kívánt viselkedés kialakításának megelőzésében.
