---
title: "Nem lehet terápiás kutyákkal intézményeket látogatni"
date: "2020-03-28"
author: "Tamás"
description: "COVID-19"
category: "Hírek"
tags:
  - hírek
  - okj
  - vizsga
---

2020\. 03. 16.-tól nem lehet terápiás kutyákkal intézményeket látogatni.

Bővebben a [MATESZE honlapján](http://www.matesze.hu/).
