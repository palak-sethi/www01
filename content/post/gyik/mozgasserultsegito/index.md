---
title: "Mi a mozgássérült segítő kutya?"
date: "2020-05-21"
description: "Mozgásában akadályozott személyeket segítő kutyák."
category: "GYIK"
tags:
  - gyik
---

Mozgássérült segítő kutyát igényelhet, ha Ön vagy gyermeke:

- mozgásában akadályozott bármilyen okból
- kerekesszékkel, mankóval, rollátorral, járókerettel közlekedik.

### Miben tud segíteni a Mozgássérültet segítő kutya?

Kerekesszékkel, vagy bottal, rollátorral közlekedő gazda mellett sétál, szükség esetén viszi a kosarat, amennyiben gazdája nem tud lehajolni, akár leesett pénzérmét, bankkártyát, leesett tárgyakat felvenni és kézbe adja, képes a gazdájától távol lévő tárgyakat irányítással kiválasztani és a gazdinak odavinni. pl. TV kapcsoló, telefon, kulcscsomó, cipő, kesztyű, póráz stb.

Kérésre föl, vagy lekapcsolja a villanyt (amennyiben eléri a kapcsolót) a gazda otthonában megfelelően kialakított ajtót kinyitja, becsukja. Lehúzza a zoknit, kesztyűt, kabátot, segít levenni a pulóvert, lehúzza a cipzárt, kihúzza és betolja a fiókot, izomgyengeség esetén a leejtett kezet visszateszi a gazda ölébe. Mindezek mellett lelki segítséget nyújt, önzetlen szeretetével hű társa gazdájának. Segít a kapcsolatteremtésben más emberekkel.
Mozgássérültet segítő kutya igénylés itt.

Részletesebben olvashat [róluk itt](/tudastar/mozgasserultsegito).
