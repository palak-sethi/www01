---
title: "Mennyibe kerül egy segítőkutya kiképzése?"
date: "2020-05-21"
description: "Terápiás 1 M Ft, személyi segítő 1,5-2 M Ft, Mozgássérültet segítő 2 M Ft, rohamjelző 2-3 M Ft."
category: "GYIK"
tags:
  - gyik
---

Terápiás 1 M Ft, személyi segítő 1,5-2 M Ft, Mozgássérültet segítő 2 M Ft, rohamjelző 2-3 M Ft.
