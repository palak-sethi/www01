---
title: "Milyen fajta alkalmas segítőkutyának?"
date: "2020-05-21"
description: "Leggyakrabban labrador, golden, uszkár, ezek keveréke, border collie, de más fajták egyedei is alkalmasak lehetnek – pumi, mudi, lagotto, spániel, német juhász stb."
category: "GYIK"
tags:
  - gyik
---

Leggyakrabban labrador, golden, uszkár, ezek keveréke, border collie, de más fajták egyedei is alkalmasak lehetnek – pumi, mudi, lagotto, spániel, német juhász stb.
