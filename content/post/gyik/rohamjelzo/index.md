---
title: "Mi a rohamjelző kutya"
date: "2020-05-21"
description: "Különféle rohamszerű állapotok jelzésére kiképzett kutya."
category: "GYIK"
tags:
  - gyik
---

Rohamjelző kutyát igényelhet, ha Ön vagy gyermeke:

- cukorbeteg (1-es vagy 2-es típusú) és gyakran ingadozik vércukorszintje
- epilepsziás, sem gyógyszerrel, sem műtéti beavatkozással, vagy egyéb módszerrel nem vált tünetmentessé, és tüneteinek száma napi kettő és három havonta egy közé esik
- más olyan betegségben szenved, melynek következtében váratlan, rohamszerű állapotromlás következik be (pl. szívbetegség, pánikbetegség)

### Miben tud segíteni?

Nagy biztonsággal jelzi előre a rosszulléteket, így a gazdi biztonságba helyezheti magát, hívhat segítséget, beveheti a gyógyszerét, elkerülheti a baleseteket, sérüléseket. Szükség esetén rosszullét közben segítséget hív a kutya, illetve ő maga is segítséget nyújt, bökdöséssel, nyaldosással stimulálja a gazdit.
Rohamjelző kutya igénylés itt.

Részletesebben olvashat [róluk itt](/tudastar/rohamjelzo).
