---
title: "Lehet-e más háziállatom segítőkutya mellett?"
date: "2020-05-21"
description: "Több mindentől függ"
category: "GYIK"
tags:
  - gyik
---

Természetesen lehet, viszont ezt fontos előre tudni, mert jelezni kell
a kiképző felé, hogy ő fel tudja készíteni erre a segítőkutyát 
a kiképzés során.
