---
title: "Csak törzskönyvezett, fajtatiszta, vagy keverék kutya is lehet alkalmas segítőkutya?"
date: "2020-05-21"
description: "Lehet keverék is. Mindig az egyed számít!"
category: "GYIK"
tags:
  - gyik
---

Lehet keverék is. Mindig az egyed számít!
