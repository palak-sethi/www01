---
title: "Mennyi ideig tart egy segítőkutya kiképzése?"
date: "2020-05-21"
description: "Terápiás vagy személyi segítő 12-15 hó, Mozgássérült segítő, Jelzőkutya 15-18 hó."
category: "GYIK"
tags:
  - gyik
---

Terápiás vagy személyi segítő 12-15 hó, Mozgássérült segítő, Jelzőkutya 15-18 hó.
