---
title: "Támogatás"
date: "2020-06-13"
description: "Segíts minket és munkánkat!"
category: "Támogatás"
tags:
  - támogatás
---

Az AURA Segítő Kutya Alapítványt és munkánktat többféleképpen 
is Tudod támogatni.

### Adó 1%

Adószámunk: **18995736-1-09**

Támogasd az AURA alapítványt személyi jövedelem adód egy százalékával. 
További útmutatót a támogatáshoz a 
[NAV oldalán találhat](https://www.nav.gov.hu/nav/szja/szja).

### AURA Önkéntes Program

Az AURA önkénteseket mindig szívesen vár. Az önkéntesek különféle
programokban vehetnek részt, mindenki segítségét, akár más
szakterület szakértelmét is szívesen várjuk! 


----

### Feltöltés alatt

![Feltöltés alatt](../../../src/images/photo/under-construction.jpg)

----
