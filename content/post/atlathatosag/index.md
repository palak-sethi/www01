---
title: "Átláthatóság"
date: "2020-03-06"
description: "Az alapítványunkról"
category: "Átláthatóság"
tags:
  - átláthatóság
---

### Pályázataink:

- [GINOP-5.1.7.17-2019-00221](/atlathatosag/ginop-2019-10-01)

### Feltöltés alatt

----

![Feltöltés alatt](../../../src/images/photo/under-construction.jpg)

----
