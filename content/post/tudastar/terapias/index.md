---
title: "Terápiás kutya"
date: "2020-06-20"
description: "A gyógypedagógiai, a szociális szolgáltatások területén pedagógiai, pszichológiai, pszichiátriai, konduktív pedagógiai habilitációs, illetve rehabilitációs folyamatban alkalmazott kutya."
category: "Tudástár"
tags:
  - tudastar
---

A gyógypedagógiai, a szociális szolgáltatások területén pedagógiai,
pszichológiai, pszichiátriai, konduktív pedagógiai habilitációs, illetve rehabilitációs 
folyamatban alkalmazott kutya. 

A legfontosabb különbség az eddig tárgyalt segítőkutya típusokhoz
képest az, hogy amíg a segítőkutyák gazdáikkal élnek, akik egyben a segített személyek, 
követik őket a nap 24 órájában mindenhová – hiszen a fogyatékkal élők nem csak egy adott 
napszakban szorulnak segítségre – addig a terápiás kutyák kizárólag a terápia alatt találkoznak a
segítendő személyekkel, és kizárólag „munkavégzés céljából” (pl tanítási időben) lehetnek
jelen egy-egy intézményben. 

A terápiás kutya gazdájával együtt dolgozik, ketten vesznek részt
csoportos vagy egyéni foglalkozásokon, segítve a fejlesztő foglalkozások kivitelezésében a
csoport (gyógy)pedagógusát, azaz az adott csoportot vezető kompetens szakembert, vele 
szorosan együttműködve, team munkában. A terápiás kutya tevékenységének is vannak fizikai és
pszichés aspektusai. A terápiás csoport tagjai számára az egyébként unalmas vagy nehéznek
tűnő feladatok elvégzése kellemes élménnyé válik, ha azt egy kutya segítségével vagy annak
jelenlétében kell megvalósítani. A kutya arra van kiképezve, hogy nem csak a felvezető 
utasításait hajtja végre, hanem a kliensek által is irányítható, még akkor is, ha azok nehézkesen,
nehezen érthetően beszélnek, vagy mozognak. A feladatok végrehajtása, amikor csak 
lehetséges közösen történik, a kutyával együtt dolgoznak (pl. a megfelelő színű labdát kiválasztani,
akadálypályát a gyerekekkel együtt teljesíteni, úgy, hogy a póráz a gyerekek kezében van
stb.). 

A terápiás kutyák alkalmazása nagyon sokrétű, a mozgásfejlesztésen át az olvasás 
tanulásáig minden területen tud egy jól képzett kutya segíteni, így a célcsoportok is változatosak, a
korai fejlesztéstől a pszichiátriai betegek kezelésén át az idősek otthonáig gyakorlatilag 
korlátlanok a lehetőségek. A kétlépcsős vizsgán a páros bizonyítja, hogy a gazda képes a kutyát a
legváltozatosabb -- minőség, mennyiség és intenzitás tekintetében -- ingereknek kitéve is 
kontrollálni valamint, hogy a kutya semmilyen körülmények között, a legváratlanabb helyzetben
sem reagál agresszívan. Ami a pszichológiai aspektust illeti, a csoportok tagjai sokszor szoros
érzelmi kötődést élnek meg a kutyával kapcsolatban, ami egy rendkívül érzékeny és sok 
felelősséggel járó mozzanata a dolognak: a kutya váratlan kivonása a csoportból a gyerekek 
számára traumát okozhat. 

A kutyás terápia eredményességét segíti az is, hogy a kutya elfogadó és
türelmes a csoport tagjaival, korlátaik ellenére dolgozik velük együtt, ráadásul megcsinálja a
feladatot, végrehajtja a sokszor messze nem tökéletes utasítást is, ami a kompetencia érzetüket
növeli. (Babos, 2013; Köböl, Hevesi és Topál 2013; Harkai, 2015; Szigeti, 2007)

#### Hivatkozások

1. Babos Edit (2013): Állatasszisztált terápia – módszertani előtanulmány óvodáskorú gyerekek 
   kutyával történő fejlesztésére. Alkalmazott Pszichológia, 13. 3. sz. 59-81.
2. Köböl Erika, Hevesi Tímea Mária és Topál József (2013): Állatasszisztált foglalkozások.
   „Mentor(h)áló 2.0 Program” TÁMOP-4.1.2.B.2-13/1-2013-0008 projekt. On-line megtekintés:
   http://www.jgypk.hu/mentorhalo/tananyag/Allatasszisztalt_foglalkozasV2/index.html
3. Harkai Viktória (2015): Az állat-tartás és az állatasszisztált terápia jótékony hatásai, 
   Gerinces Magazin, on-line megtekintés: https://gerinces.hu/pszichologia/az-allat-tartas-es-az-allatasszisztalt-terapia-jotekony-hatasai/
4. Szigeti Mónika (2007): Kutyás terápia alkalmazása kommunikációs zavarokkal küzdő 
   gyermekeknél. Fejlesztő Pedagógia, 18. 3-4. sz. 67-70.
