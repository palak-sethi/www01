---
title: "Hangjelző kutya"
date: "2020-06-20"
description: "A hallássérült személy számára a különféle, elsősorban veszélyt jelző vagy egyéb kiemelten informatív hangok jelzésére kiképzett kutya."
category: "Tudástár"
tags:
  - tudastar
---

A hallássérült személy számára a különféle, elsősorban veszélyt jelző vagy egyéb kiemelten 
informatív hangok jelzésére kiképzett kutya. A hallókutyák hatékony
segítséget tudnak nyújtani a siket vagy nagyothalló gazdának, azáltal, hogy folyamatosan a
közelében vannak, és különböző módon jelzik pl. a kopogást, telefoncsörgést, ébresztőóra
vagy a riasztó hangját. 

A kicsi gyermekükkel sok időt kettesben eltöltő siket anyukáknak 
jelentős támogatás, hogy nem kell folyamatosan a gyermeken tartani a szemüket, hanem a kutya
jelez, ha a gyermek sír, vagy elindul valamerre, esetleg veszélyes tevékenységbe kezd. A
hangjelző kutyáknál fontos hangsúlyozni, hogy ellentétben a többi segítőkutyával, nekik jóval
nehezebb dolguk van, mert nem elsősorban a gazda jelzéseire kell figyelni, hanem a különféle
környezeti tényezőkre, eseményekre. Náluk az önálló döntéshozatalra való képesség, az 
önállóság így kiemelt szerephez jut. Továbbá ők, ellentétben például vakvezető vagy mozgássérült
segítő társaikkal állandó készenléti állapotban kell, hogy legyenek, ami jóval nagyobb 
igénybevételt jelent. Nagyon fontos az extrém erős kötődés, ami garantálja, hogy a kutya 
ténylegesen állandó jelleggel a gazda mellett legyen. (KEA, 2019)

#### Hivatkozások:

1. KEA Kutyával az Emberért Alapítvány honlapja, On-line megtekintés: http://www.kea-net.hu/?page=hangjelzokutyak
